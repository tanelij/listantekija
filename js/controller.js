(function(){
    'use strict'

    function Controller(model, view){
        var that = this;
        that.model = model;
        that.view = view;

        that.view.bind('addChild', function (item) {
            that.view._addChild();
        });
    }


    window.app = window.app || {};
    window.app.Controller = Controller;
})();