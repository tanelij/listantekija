(function(){
    function ListMaker(name){
        this.view = new app.View(this.template);
        this.controller = new app.Controller(this.model, this.view);
    }

    listMaker = new ListMaker('listmaker');

})();