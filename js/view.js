/**
 * Created by tanel on 8.5.2017.
 */
(function(){

    function View(template) {
        this.template = template;

        this.ENTER_KEY = 13;
        this.ESCAPE_KEY = 27;

        this.$main = qs('#main');
        this.$toggleAll = qs('#toggle-all');

        this.$addUl = qs('#add-ul');
        this.$addChild = qs('#add-child');
        this.list;
        this.selectedNodes = [];

        var that = this;

        var option = qs('#option');
        var settings = qs('#settings');
        var listTypeSelect = qs('#list-type');

        console.log(listTypeSelect);
        console.log(listTypeSelect.value);
        this.listType = listTypeSelect.value;

        //list of valid children to add to different type of lists
        this.childs = {
            UL: 'li',
            OL: 'li',
            DL: 'dt',
            DT: 'dd'
        };

        //Create the options panel with a nodeparser, and add event handlers for the different options
        var options = (function(){
            var parser = new DOMParser();
            var nodeStrings
                = '<input type="range" id="font" min="10" max="40">'
                + '<input id="color" type="color">'
                + '<input id="bcolor" type="color">'
                + '<select id="fonts">'
                +   '<option value="0">Arial</option>'
                +   '<option value="1">Comic Sans</option>'
                +   '<option value="2">Impact</option>'
                +   '<option value="3">Times New Roman</option>'
                + '</select>';
            var root =  parser.parseFromString(nodeStrings, "text/html");
            var fonts = root.getElementById("font");
            var color = root.getElementById("color");
            var bcolor =  root.getElementById("bcolor");
            var fontStyle = root.getElementById("fonts");

            //list of possible fonts
            var fontFamilies = ['Arial, Helvetica, sans-serif',
                '"Comic Sans MS", cursive, sans-serif',
                'Impact, Charcoal, sans-serif',
                '"Times New Roman", Times, serif'];

            var colorHandler = function(){
                console.log("COLORHANDLER");
                console.log("that.selectedNodes");
                console.log(that.selectedNodes);
                for(var i=0; i<that.selectedNodes.length; i++){
                    console.log("that.selectedNodes");
                    console.log(that.selectedNodes);
                    that.selectedNodes[i].style.color = this.value;
                }
            };

            var fontHandler = function(){
                for(var i=0; i<that.selectedNodes.length; i++){
                    that.selectedNodes[i].style.fontSize = this.value + "px";
                }
            };

            var bcolorHandler = function(){
                for(var i=0; i<that.selectedNodes.length; i++){
                    that.selectedNodes[i].style.backgroundColor = this.value;
                }
            };

            var fontStyleHandler = function(){
                for(var i=0; i<that.selectedNodes.length; i++){
                    console.log("that.selectedNodes");
                    console.log(that.selectedNodes);
                    that.selectedNodes[i].style.fontFamily = fontFamilies[this.value];
                }
            };


            $on(color, 'change', colorHandler);
            $on(fonts, 'change', fontHandler);
            $on(bcolor, 'change', bcolorHandler);
            $on(fontStyle, 'change', fontStyleHandler);

            return{
                color:  color,
                bcolor:  bcolor,
                fonts: fonts,
                fontStyle: fontStyle
            }
        }());

        //handler for the listType-panel
        //Creates the root list onto which the children get added
        var listTypeHandler = function(){
            that.$main.innerHTML = '';
            that.listType = this.value;
            var list = document.createElement(that.listType);
            that.$main.appendChild(list);
            list.id = 'list';
            that.list = list;
            that.selectedNodes = [];
            console.log("that.list");
            console.log(that.list);
        };
        listTypeHandler();
        //creates the listener for the listType-panel
        $on(listTypeSelect, 'change', listTypeHandler);

        //create the handler and listener for the settings panel
        var settingsHandler = function(){
            if(option.childNodes){
                option.innerHTML = '';
            }
            option.appendChild(options[this.value]);
        };
        $on(settings, 'change', settingsHandler);


        //create the listener and handler for the toggle-all button
        var toggleAllHandler = function(){
            var checked = this.checked;

            that.list.childNodes.forEach(function(node){
                if(!node.tagName){
                    return;
                }
                console.log(node.tagName);
                var i = that.selectedNodes.indexOf(node);
                if(!checked){
                    if(i >= 0){
                        that.selectedNodes[i].className = '';
                        that.selectedNodes.splice(i, 1);
                    }
                }else if(i<0){
                    that.selectedNodes.push(node);
                    node.className = 'selected';
                }
            });

        };
        $on(that.$toggleAll, 'click', toggleAllHandler);
        option.appendChild(options[settings.value]);

        //create the download and upload panels
        this.downloadPanel();
        this.uploadPanel();
    }


    View.prototype.uploadPanel = function(){
        // Get the modal
        var modal = document.getElementById('upload-modal');

        // Get the button that opens the modal
        var btn = document.getElementById("upload");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }


        var listObject = function(name, data){
            this.name = name;
            this.data = data;
        };

        document.getElementById('upload-form').onsubmit = postList;

        function postList() {
            var listName = document.getElementById("list-name").value;
            var data = document.getElementById("main").innerHTML;

            var list = new listObject(listName, data);
            var listJSON = JSON.stringify(list);
            
            console.log("listJSON");
            console.log(listJSON);

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if(xmlhttp.responseText){
                        console.log("xmlhttp");
                        alert(xmlhttp.responseText);
                    }
                }
            };

            xmlhttp.open("POST", "https://projekti-taneli.c9users.io/staffapi/list", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send("list=" + listJSON);
            return false;
        }

    };

    View.prototype.downloadPanel = function(){
        // Get the modal
        var modal = document.getElementById('download-modal');

        // Get the button that opens the modal
        var btn = document.getElementById("download");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[1];

        // When the user clicks the button, open the modal
        btn.onclick = function() {
            modal.style.display = "block";
            getLists();
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        function getList() {
            var id = this.value;
            var container = document.getElementById("main");
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if(xmlhttp.responseText){

                        var responseText = xmlhttp.responseText;
                        var responseList = JSON.parse(responseText);
                        container.innerHTML = responseList.data;
                    }
                }
            };

            xmlhttp.open("GET", "https://projekti-taneli.c9users.io/staffapi/list/" + id, true);
            xmlhttp.send();
            return false;
        }

        function getLists() {
            console.log("sendMessage");
            var container = document.getElementById("download-container");

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if(xmlhttp.responseText){

                        var responseText = xmlhttp.responseText;
                        console.log("responseText");
                        console.log(responseText);
                        var responseList = JSON.parse(responseText);
                        var responseData = responseList.data;
                        console.log("responseData");
                        console.log(responseData);
                        var select = document.getElementById("select-download");
                        for (var property in responseList) {
                            if (responseList.hasOwnProperty(property)) {
                                var option = document.createElement('option');
                                option.innerHTML = responseList[property]['name'];
                                select.appendChild(option);
                                option.value = responseList[property]['id'];
                            }
                        }
                        select.addEventListener('change', getList);
                        container.appendChild(select);
                    }
                }
            };

            xmlhttp.open("GET", "https://projekti-taneli.c9users.io/staffapi/lists", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send();
            return false;
        }
    };

    //Adds one child on each selected node, of if no nodes are selected add one to the main list
    View.prototype._addChild = function(){
        var that = this;

        var makeChild = function(tagname, parent){
            console.log("parent");
            console.log(parent);
            var child = document.createElement(tagname);
            child.innerHTML = 'test';
            child.class = 'item';
            parent.appendChild(child);
            var destroy = document.createElement('button');
            $on(destroy, 'click', function(){
                var i = that.selectedNodes.indexOf(this);
                if(i >= 0){
                    that.selectedNodes.splice(i, 1);
                    this.className = '';
                }
                that.list.removeChild(child);
            });

            $on(child, 'click', (function(){
                var i = that.selectedNodes.indexOf(this);
                if(i >= 0){
                    that.selectedNodes.splice(i, 1);
                    this.className = '';
                }else{
                    that.selectedNodes.push(this);
                    this.className = 'selected';
                }
            }));
            child.appendChild(destroy);
            destroy.className = 'destroy';
            $on(child, 'dblclick', that._editItem.call(child));
        };

        for(var i=0; i<that.selectedNodes.length; i++){
            if(that.childs[that.selectedNodes[i].tagName]){
                console.log("that.selectedNodes");
                console.log(that.selectedNodes);
                makeChild(that.childs[that.selectedNodes[i].tagName], that.selectedNodes[i]);
            }
        }
        if(that.selectedNodes.length === 0){
            if(that.childs[that.listType]){
                makeChild(that.childs[that.listType], that.list);
            }
        }

    };

    //event handler for editing an list-item
    View.prototype._editItem = function(){
        return function(){
            this.className = this.className + ' editing';

            var input = document.createElement('input');
            input.className = 'edit';
            this.appendChild(input);
            var escape = false;
            $on(input, 'blur', (function(){
                if(escape){
                    escape = false;
                    this.remove();
                    return;
                }
                this.parentNode.className='';
                this.parentNode.textContent = this.value;
                this.remove();
            }));
            $on(input, 'keypress', (function(){
                if(event.keyCode===13){
                    this.parentNode.className='';
                    this.parentNode.textContent = this.value;
                    this.remove();
                }
            }));
            $on(input, 'keyup', (function(){
                if(event.keyCode === 27){
                    escape=true;
                    this.blur()
                }
            }));
            input.focus();
            input.value = this.textContent;
        };
    };

    View.prototype.bind = function(event, handler){
        var that = this;
        if(event === 'addChild'){
            $on(that.$addChild, 'click', handler);
            console.log("event === 'addChild'");
        }

    };

    // Export to window
    window.app = window.app || {};
    window.app.View = View;
})();