/**
 * Created by tanel on 8.5.2017.
 */
(function(){
    'use strict';

    window.$on = function (target, type, callback, useCapture){
        target.addEventListener(type, callback, !!useCapture);
    };
    window.qs = function(selector, scope){
        return (scope || document).querySelector(selector);
    };
    window.qsa = function(selector, scope){
        return (scope || document).querySelectorAll(selector);
    };


})(window);