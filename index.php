<?php

# URI parser helper functions
# ---------------------------


$servername = "localhost";
$username = "lists";
$password = "lists";

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 



    function getResource() {
        # returns numerically indexed array of URI parts
        $resource_string = $_SERVER['REQUEST_URI'];
        if (strstr($resource_string, '?')) {
            $resource_string = substr($resource_string, 0, strpos($resource_string, '?'));
        }
        $resource = array();
        $resource = explode('/', $resource_string);
        array_shift($resource);   
        return $resource;
    }

    function getParameters() {
        # returns an associative array containing the parameters
        $resource = $_SERVER['REQUEST_URI'];
        $param_string = "";
        $param_array = array();
        if (strstr($resource, '?')) {
            # URI has parameters
            $param_string = substr($resource, strpos($resource, '?')+1);
            $parameters = explode('&', $param_string);                      
            foreach ($parameters as $single_parameter) {
                $param_name = substr($single_parameter, 0, strpos($single_parameter, '='));
                $param_value = substr($single_parameter, strpos($single_parameter, '=')+1);
                $param_array[$param_name] = $param_value;
            }
        }
        return $param_array;
    }

    function getMethod() {
        # returns a string containing the HTTP method
        $method = $_SERVER['REQUEST_METHOD'];
        return $method;
    }
 
# Handlers
# ------------------------------
	
	function getList($id){
	   
	    global $conn;

	    
        if (!mysqli_select_db($conn, 'lists')) {
            echo("Uh oh, couldn't select database ");
            return;
        }
	    
       if (!$stmt = $conn->prepare("SELECT * FROM lists WHERE id = ?")) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
            return;
        }
        
        if (!$stmt->bind_param('s', $id)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return;
        }
        
        
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return;
        }

        $res = $stmt->get_result();
        $row = $res->fetch_assoc();
        
        echo json_encode($row);
	}
	
	function getLists(){
	    global $conn;
	    
        if (!mysqli_select_db($conn, 'lists')) {
            echo("Uh oh, couldn't select database ");
            return;
        }
	    
       if (!$stmt = $conn->prepare("SELECT * FROM lists")) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
            return;
        }
        
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return;
        }

        $res = $stmt->get_result();
        //$row = $res->fetch_assoc();
        $a = [];
        while ($row = $res->fetch_assoc()) {
            //echo $row['name'] ." " .$row['data'] ."\n       ";
            $a[$row['id']] = $row;
        }
        
        echo json_encode($a);
	}
	
	function postList($parameters){
	    global $conn;
        $list = $_POST['list'];
        $list = json_decode($list);
        if(($list->name && is_string($list->name)) &&  ($list->data && is_string($list->data)) && $conn){
            
            if (!mysqli_select_db($conn, 'lists')) {
                echo("Uh oh, couldn't select database ");
                return;
            }
            
            if (!($stmt = $conn->prepare("INSERT INTO lists(name,data) VALUES (?,?)"))) {
                echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
                return;
            }
            
            if (!$stmt->bind_param('ss', $list->name, $list->data)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
                return;
            }
            
            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                return;
            }
            
            echo "list uploaded succesfully!";
        }else{
            echo "improper JSON";
        }

	}


# Main
# ----

	$resource = getResource();
    $request_method = getMethod();
    $parameters = getParameters();
    

    # Redirect to appropriate handlers.
	if ($resource[0]=="staffapi") {
        if($request_method=="POST" && $resource[1]=="list"){
    		postList($parameters);
    	}
    	else if ($request_method=="GET" && $resource[1]=="lists") {
			getLists();
		} 
		else if ($request_method=="GET" && $resource[1]=="list") {
			getList($resource[2]);
		} 
		else {
			http_response_code(405); # Method not allowed
		}
	}
	else {
		http_response_code(405); # Method not allowed
	}
?>

